package com.students.bookreservation.dto;

import com.students.bookreservation.entities.Book;
import com.students.bookreservation.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReservationDTO {

    private Integer id;
    private Book book;
    private User user;
    private Long timeReservation;

}
