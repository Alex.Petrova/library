package com.students.bookreservation.controllers;

import com.students.bookreservation.dto.BookDTO;
import com.students.bookreservation.entities.Book;
import com.students.bookreservation.service.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDTO> getBookById(@PathVariable("id") Integer id) {
        final Book book = bookService.find(id);
        if (book != null) {
            return ResponseEntity.ok(new BookDTO(book.getId(), book.getTitle(), book.getAuthor()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<BookDTO> saveBook(@RequestBody BookDTO bookDTO) {
        final Book book = new Book();
        book.setAuthor(bookDTO.getAuthor());
        book.setTitle(bookDTO.getTitle());
        bookService.save(book);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/all")
    public List<BookDTO> findAllBook(){
       return bookService.findAll();
    }
}
