package com.students.bookreservation.controllers;

import com.students.bookreservation.dto.ReservationDTO;
import com.students.bookreservation.entities.Reservation;
import com.students.bookreservation.service.ReservationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reservation")
public class ReservationController {

    private final ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping("/{idBook}")
    public ResponseEntity<ReservationDTO> getUserByBookId(@PathVariable("idBook") Integer idBook) {
        final Reservation reservation = reservationService.find(idBook) ;
        if (reservation != null) {
            return ResponseEntity.ok(new ReservationDTO(reservation.getId(),reservation.getBook(), reservation.getUser(),reservation.getTimeReservation()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/addBookMyLibrary")
    public ResponseEntity<ReservationDTO> saveBook(@RequestBody ReservationDTO reservationDTO){

        final Reservation reservation = new Reservation();
        reservation.setBook(reservationDTO.getBook());
        reservation.setUser(reservationDTO.getUser());

        if(reservationService.find(reservation.getBook().getId()) == null) {
            reservationService.save(reservation);
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.badRequest().build();
        }
    }
}
