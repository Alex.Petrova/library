package com.students.bookreservation.repositories;

import com.students.bookreservation.dto.BookDTO;
import com.students.bookreservation.entities.Book;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository  {

    Book find(Integer id);

    List<BookDTO> findAll();

    void save(Book book);

    void update(Book book);
}
