package com.students.bookreservation.repositories;

import com.students.bookreservation.entities.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository {

    User find(Integer id);

    void save(User user);

    void update(User user);

}
