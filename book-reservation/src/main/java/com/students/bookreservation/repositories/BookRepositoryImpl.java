package com.students.bookreservation.repositories;
import com.students.bookreservation.dto.BookDTO;
import com.students.bookreservation.entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class BookRepositoryImpl implements BookRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Book find(Integer id) {
        return entityManager.find(Book.class, id);
    }

    @Override
    public List<BookDTO> findAll() {
        return entityManager.createQuery("from Book", BookDTO.class).getResultList();
    }

    @Override
    public void save(Book book) {
        entityManager.getTransaction().begin();
        entityManager.persist(book);
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(Book book) {
        entityManager.merge(book);
    }
}
