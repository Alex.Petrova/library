package com.students.bookreservation.repositories;

import com.students.bookreservation.entities.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class ReservationRepositoryImpl implements ReservationRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Reservation find(Integer book_id) {
        return entityManager.find(Reservation.class, book_id);
    }

    @Override
    public void save(Reservation reservation) {
        entityManager.getTransaction().begin();
        entityManager.persist(reservation);
        entityManager.flush();
        entityManager.getTransaction().commit();

    }

    @Override
    public void update(Reservation reservation) {
        entityManager.merge(reservation);
    }

    @Override
    public void delete(Integer book_id) {
        Reservation reservation = entityManager.find(Reservation.class, book_id);
        entityManager.remove(reservation);
    }

    @Override
    public List<Reservation> findAll() {
        return entityManager.createQuery("from Reservation", Reservation.class).getResultList();
    }
}
