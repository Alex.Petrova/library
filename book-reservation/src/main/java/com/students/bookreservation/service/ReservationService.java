package com.students.bookreservation.service;

import com.students.bookreservation.dto.ReservationDTO;
import com.students.bookreservation.entities.Reservation;

import java.util.List;

public interface ReservationService {

    Reservation find(Integer book_id);

    void save(Reservation reservation);

    void update(Reservation reservation);

    void delete(Integer book_id);

    List<Reservation> findAll();

}
