package com.students.bookreservation.service;

import com.students.bookreservation.entities.User;

public interface UserService {

    User find(Integer id);

    void save(User user);

    void update(User user);

}
