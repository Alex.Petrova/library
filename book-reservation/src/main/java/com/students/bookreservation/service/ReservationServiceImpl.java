package com.students.bookreservation.service;

import com.students.bookreservation.dto.ReservationDTO;
import com.students.bookreservation.entities.Reservation;
import com.students.bookreservation.repositories.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService{
    @Autowired
    private ReservationRepository reservationRepository;

    @Override
    public Reservation find(Integer book_id) {
        return reservationRepository.find(book_id);
    }

    @Override
    public void save(Reservation reservation) {
        reservationRepository.save(reservation);
    }

    @Override
    public void update(Reservation reservation) {
        reservationRepository.update(reservation);
    }

    @Override
    public void delete(Integer book_id) {
        reservationRepository.delete(book_id);
    }

    @Override
    public List<Reservation> findAll() {
        return reservationRepository.findAll();
    }
}
